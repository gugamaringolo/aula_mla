const {createApp} = Vue;
createApp
({
    data()
    {
        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
        };//fechamento do return
    },//fechamento da função "data"

    methods:
    {
        handleButtonclick(botao){
            switch(botao){
                case "+":
                case "-":
                case "/":
                case "*":
                    console.log(botao);
                    this.handleOperation(botao);
                    break; 
                    case ".":
                        this.handleDecimal();
                        break;     
                    case "=":
                        this.handleEquals();
                        break;  
                default:
                    this.handleNumber(botao);
                    break;
            }//fechamento switch
        },//fechamento handleButtonclick

        handleNumber(numero){
            if(this.display === "0")
            {
                this.display = numero.toString();
            }
            else
            {
                this.display += numero.toString();
                /*this.display = this.display + numero.toString();*/
            }
        },//fechamento handlenumbe

        handleOperation(operacao){
            if(this.operandoAtual !== null){
                this.handleEquals();
            }//fechamento do if
            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";

        },//fechamento handleOperation

        handleEquals(){
            const displayAtual = parseFloat(this.display);
            if(this.operandoAtual !== null && this.operador !== null){
                switch(this.operador){
                    case "+":
                        this.display = (this.operandoAtual + displayAtual).toString();
                        break;
                        case "-":
                            this.display = (this.operandoAtual - displayAtual).toString();
                            break;
                            case "*":
                            this.display = (this.operandoAtual * displayAtual).toString();
                            break;
                            case "/":
                                this.display = (this.operandoAtual / displayAtual).toString();
                                break;
                }//fechamento do switch

                this.operandoAnterior = this.operandoAtual;
                this.operandoAtual = null;
                this.operador = null;
            
            }//fechamento do if
            else
            {
                this.operador = displayAtual;
            }


        },//fechamento handleEquals

        handleDecimal(){
            if(!this.display.includes("."))
            {
                this.display += ".";
            }//fechamento do if
        },//fechamento handleDecimal

        handleClear(){
            this.display = "0";
            this.operandoAtual = null;
            this.operandoAnterior = null;
            this.operador = null;
        },//fim handleClear
    },//fim methods 
    
}).mount("#app"); //fechamento do "createApp"