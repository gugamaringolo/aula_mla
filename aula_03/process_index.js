const {createApp} = Vue;

createApp({
    data(){
        return{
            numero:0,
        };
    },

    methods:{
        adicionarValor:function(){
            this.numero++;
        }

    }

}).mount("#app");