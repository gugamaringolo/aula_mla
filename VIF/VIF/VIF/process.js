const {createApp} = Vue;
createApp({
    data(){
        return{
            testeSpan: false,
            isLampadaligada: false,
        };//fim return
    },//fim data

    methods:{
        handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
            this.isLampadaligada = !this.isLampadaligada;
        },//fim haldletest
    },//fim methods
}).mount("#app")