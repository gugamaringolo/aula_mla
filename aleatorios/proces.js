const {createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagenslocais:[
                './imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'
            ],
            imagensInternet:[
                'https://www.gratistodo.com/wp-content/uploads/2017/08/Los-Simpson-Wallpapers-1.jpg',
                'https://http2.mlstatic.com/papel-parede-adesivo-hd-flamengo-22-x-13-m-frete-gratis-D_NQ_NP_800408-MLB26423101010_112017-F.jpg',
                'https://wallpapercave.com/wp/wp6966545.jpg'
            ]
        };//fim return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagenslocais[this.randomIndex];
        },//fim randomIma
        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        },//random image
    },//fim computed
    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagenslocais.length);
            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//fim methods
}).mount("#app");