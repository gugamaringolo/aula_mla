const{createApp} = Vue;

createApp({
    data(){
        return{
            username: '',
            password: '',
            error: null,
            sucesso: null,

            //arrays para armazenamento
            usuarios: ["admin", "Gustavo"],
            senhas: ["1234", "1234"],

            newUsername:"",
            newPassword:"",
            confirmPassword:"",

            mostrarEntrada: false,

            mostrarLista: false,

        }// fechamento return
    },// fechamento data

    methods:{
        login(){
            setTimeout(() => {
                if((this.username === 'Gustavo' && this.password === '12342023') || (this.username === 'leo' && this.password === '15161718')){
                    alert("login realizado com sucesso!");
                    this.error = null;
                }// fim do if
                else{
                    this.error = "Nome ou senha incorretos!";
                }// fim do else
            }, 1000);
        },// fechamento de login

        loginArray(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    this.error = null;
                    window.location.href = "";

                    localStorage.setItem("username", this.username);
                    localStorage.setItem("paassword", this.password);

                } 
                else{
                    this.sucesso = null;
                    this.error = "Usuário ou senha incorretos!"; 
                }

                this.username = "";
                this.password = "";
            }, 500)

        },//fechamento login

        adicionarUsuario(){
            this.mostrarEntrada = false;

            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){
                    if(this.newPassword && this.newPassword === this.confirmPassword){
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);
                        
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                        this.newUsername = "";
                        this.newPassword = "";
                        this. confirmPassword = "";
                        this.sucesso = "Usuario cadastrdo com sucesso!";
                        this.error = null;
                    }
                    else{
                        this.error = "por favor, informe uma senha valida";
                        this.sucesso = null;
                    }
                }// fechamento if
                else{
                    this.error = "por favor, informe uma usuário válido";
                    this.sucesso = null;
                }// fechamento else
            }, 500);
        },//fechamento adicionarUsuario
        paginaCadastro(){
            this.mostrarEntrada = false
            setTimeout(() => {
                this.mostrarEntrada = true;
                this.sucesso = "carregando a pagina de cadastro!"; 
            }, 500);

            setTimeout(() => {
                window.location.href = "cadastro.html";
            }, 1000);
        },
        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            this.mostrarLista = !this.mostrarLista;
        },
    },// fechamento methods

}).mount("#app");// fechamento createApp