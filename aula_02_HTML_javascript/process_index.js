//processamento dos dados dof formulario "index.html"

// A cessando os elementos formulario do html
const formulario = document.getElementById("formulario");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function(evento)
{
    evento.preventDefault();/*previne o compartimento padrão de um elelmento HTML em respota a um evento*/

    //constante para tratar osdadodos recbidos dos elementos do formulario
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //exibe um alerta com os dados coletados
    //alert(`Nome: ${nome} --- E-mail: ${email}`);

    const updateResultado = document.getElementById("resultado");

    updateResultado.value = `nome: ${nome} ---  E-mail: ${email}`

    updateResultado.style.width = updateResultado.scrollWidth + "px";
});